﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;

namespace Hont.AStar
{
    public class HontAStarUnitySeeker : MonoBehaviour
    {
        public int mask = -1;
        public bool isDebugPathPoint = true;
        public Action<Vector3[]> OnPathfindingFinishedCB;

        HontAStarUnityPathPointDebuger mPathPointDebuger;


        public Vector3[] StartPathfinding(Vector3 beginPos, Vector3 endPos, Action<Vector3[]> onPathfindingFinishedCB = null)
        {
            Vector3[] result = null;
            var useableAStarArr = HontAStarUnity.CreatedAStarList.Where(m => m.IsInitialized);

            var targetAStar = useableAStarArr.FirstOrDefault(m => m.LocalBounds.Contains(m.transform.InverseTransformPoint(endPos)));

            if (targetAStar)
                result = targetAStar.StartPathfinding(beginPos, endPos, mask);

            if (onPathfindingFinishedCB != null)
                onPathfindingFinishedCB(result);

            if (OnPathfindingFinishedCB != null)
                OnPathfindingFinishedCB(result);

            if (isDebugPathPoint)
            {
                if (mPathPointDebuger == null)
                    mPathPointDebuger = HontAStarUnityHelper.CreatePathfindingPathDebuger(result);

                mPathPointDebuger.pathArr = result;
            }

            return result;
        }
    }
}
