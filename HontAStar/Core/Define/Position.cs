﻿using UnityEngine;
using System.Collections;

namespace Hont.AStar
{
    public struct Position
    {
        public static readonly Position Default = default(Position);
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }


        public Position(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public override string ToString()
        {
            return string.Format("X: {0} Y: {1} Z: {2}", X, Y, Z);
        }
    }
}

